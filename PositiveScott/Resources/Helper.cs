﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls.Expressions;

namespace PositiveScott.Resources
{
    public static class Helper
    {
        public static IHtmlString HTMLFieldNew(string ValName, int ValID, int ValType, int PluginType, string FriendlyName, string AppID, string Mandatory = "", string DDValues = "", string onClick = "", string OnChange = "", string OnKeyPress = "", string ToolTip = "")
        {


            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            switch (ValType)
            {
                case 55:
                    //// Accordion Group Open
                    htmlBuilder.Append("<div class=\"form- group row\">");
                    htmlBuilder.Append("<label class=\"control-label text-left col-md-5\">"+ FriendlyName +"</label>");
                    htmlBuilder.Append("<div class=\"col-md-7\">");
                    htmlBuilder.Append("<input class=\"form-control\" id=\""+ ValName + "\" name=\""+ ValName + "\" onblur=\"saveField("+ AppID +", this.id, this.value); \" type=\"text\">");
                    htmlBuilder.Append("</div>");
                    htmlBuilder.Append("</div>");

                    break;
            }

            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }


        public static string GetTagType(int Type)
        {
            string ResultingType = "";

            switch (Type)
            {
                case 1:
                    ResultingType = "input";
                    break;
                case 2:
                    ResultingType = "select";
                    break;
                case 3:
                    ResultingType = "date";
                    break;
                case 4:
                    ResultingType = "textarea";
                    break;
                case 5:
                    ResultingType = "radio";
                    break;
                case 6:
                    ResultingType = "checkbox";
                    break;
            }
            return ResultingType;
        }

        public static IHtmlString HTMLField(int Type, string Name, string ID, string AppID, string DDValues = "")
        {
            var html = "";

            var ContainerDiv = new TagBuilder("div");
            ContainerDiv.AddCssClass("form-group row");
            var label = new TagBuilder("label");
            label.SetInnerText(Name);
            label.AddCssClass("control-label text-left col-md-5");
            var inputContainer = new TagBuilder("div");
            inputContainer.AddCssClass("col-md-7");
            var fieldinput = GetTag(GetTagType(Type), ID, Name, AppID, DDValues).ToHtmlString();
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.Append(label.ToString(TagRenderMode.Normal));
            htmlBuilder.Append(inputContainer.ToString(TagRenderMode.StartTag));
            htmlBuilder.Append(fieldinput.ToString());
            htmlBuilder.Append(inputContainer.ToString(TagRenderMode.EndTag));
            ContainerDiv.InnerHtml = htmlBuilder.ToString();
            html = ContainerDiv.ToString(TagRenderMode.Normal);
            return MvcHtmlString.Create(html);
        }

        public static IHtmlString GetTag(string Type, String ID, String Name, string AppID, string DDValues = "")
        {
            var NewType = "";
            if (Type == "date")
            {
                NewType = "input";
            }
            else
            {
                NewType = Type;
            }



            if (Type == "radio")
            {


            }
            else if (Type == "checkbox")
            {

            }
            else
            {




            }

            var Tag = new TagBuilder(NewType);
            Tag.AddCssClass("form-control");
            Tag.Attributes.Add("id", Name);
            Tag.Attributes.Add("name", Name);
            Tag.Attributes.Add("type", "text");
            Tag.Attributes.Add("onblur", "saveField(" + AppID + ", this.id, this.value);");

            if (Type == "select")
            {
                string[] DDVals = DDValues.Split(new string[] { "|" }, StringSplitOptions.None);
                foreach (string i in DDVals)
                {
                    TagBuilder Option = new TagBuilder("option");
                    if (i == "Please Select")
                    {
                        Option.MergeAttribute("value", "");
                    }
                    else
                    {
                        Option.MergeAttribute("value", i);
                    }
                    Option.InnerHtml = i;
                    Tag.InnerHtml += Option.ToString();
                }
            }

            if (Type == "date")
            {
                Tag.Attributes.Remove("type");
                Tag.Attributes.Add("type", "date");
            }

            if (Type == "textarea")
            {
                Tag.Attributes.Add("rows", "5");
            }


            return MvcHtmlString.Create(Tag.ToString());

        }

        public static IHtmlString HTMLPlugin(int Type, string Name, string FriendlyName, string Class = "")
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            switch (Type)
            {
                case 55:
                    //// Accordion Group Open
                    htmlBuilder.Append("<div class=\"card m-b-0\">");
                    break;
                case 56:
                    //// Accordion Group Close
                    htmlBuilder.Append("</div>");
                    break;
                case 57:
                    ///Accordion Header 
                    htmlBuilder.Append("<a class=\"card-header collapsed\" id=\"AccordHeader" + Name + "\" data-toggle=\"collapse\" data-target=\"#" + Name + "\" aria-expanded=\"false\">");
                    htmlBuilder.Append("<button class=\"btn btn-link collapsed\" aria-expanded=\"false\" aria-controls=\"" + Name + "\">");
                    htmlBuilder.Append("<h5 class=\"m-b-0\">" + FriendlyName + "</h5>");
                    htmlBuilder.Append("</button>");
                    htmlBuilder.Append("</a>");
                    break;
                case 58:
                    //// Accordion Body Open
                    htmlBuilder.Append("<div id=\"" + Name + "\" class=\"collapse\" aria-labelledby=\"" + Name + "\" style=\"\">");
                    htmlBuilder.Append("<div class=\"card-body\">");
                    htmlBuilder.Append("<div class=\"form-body\">");
                    break;
                case 59:
                    //// Accordion Body Close
                    htmlBuilder.Append("</div>");
                    htmlBuilder.Append("</div>");
                    htmlBuilder.Append("</div>");
                    break;
                case 60:
                    //// Col Opens
                    htmlBuilder.Append("<div class=\"" + Class + "\">");
                    break;
                case 61:
                    //// Col Closes
                    htmlBuilder.Append("</div>");
                    break;
            }

            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }

        public static IHtmlString CommonDropdown(string FieldName, string Value = "")
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var FieldsList = from IM in db.ImportValidations
                                 where IM.ValName == FieldName
                                 select new { IM.DropdownValues };

                foreach (var Field in FieldsList)
                {


                    string[] values = Field.DropdownValues.Split('|');
                    htmlBuilder.Append("<select class=\"form-control custom-select\" id=\"" + FieldName + "\" name=\"" + FieldName + "\">");
                    foreach (var item in values)
                    {
                     
                        if(item == Value)
                        {
                            htmlBuilder.Append("<option value=\"" + item + "\" selected=\"selected\">" + item + "</option>");
                        }
                        else
                        {
                            htmlBuilder.Append("<option value=\"" + item + "\">" + item + "</option>");
                        }


                        
                        
                    }
                    htmlBuilder.Append("</select>");
                }
            }


            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }

        public static IHtmlString BrokerDropdown()
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var FieldsList = from Broker in db.Brokers
                                 select new { Broker.CompanyNumber, Broker.BrokerID };

                htmlBuilder.Append("<select class=\"form-control custom-select\" id=\"Broker\" name=\"Broker\">");
                htmlBuilder.Append("<option value=\"\">Please Select</option>");
                foreach (var Field in FieldsList)
                {
                        htmlBuilder.Append("<option value=\"" + Field.BrokerID + "\">" + Field.CompanyNumber + "</option>");
                }
                htmlBuilder.Append("</select>");
            }


            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }

        public static IHtmlString ContactDropdown()
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var FieldsList = from Contact in db.Contacts
                                 select new { Contact.ContactID, Contact.FirstName, Contact.Surname };

                htmlBuilder.Append("<select class=\"form-control custom-select\" id=\"Contact\" name=\"Contact\">");
                htmlBuilder.Append("<option value=\"\">Please Select</option>");
                foreach (var Field in FieldsList)
                {
                    htmlBuilder.Append("<option value=\"" + Field.ContactID + "\">" + Field.FirstName + " " + Field.Surname + "</option>");
                }
                htmlBuilder.Append("</select>");
            }


            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }





        public static IHtmlString NationalityDropdown(string value = "")
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var FieldsList = from Nationality in db.Nationalities
                                 select new { Nationality.NationalityID, Nationality.Nationality1};

                htmlBuilder.Append("<select class=\"form-control custom-select\" id=\"Contact\" name=\"Contact\">");
                htmlBuilder.Append("<option value=\"\">Please Select</option>");
                foreach (var Field in FieldsList)
                {
                    if (value == Field.Nationality1)
                    {
                        htmlBuilder.Append("<option value=\"" + Field.Nationality1 + "\" selected=\"selected\">" + Field.Nationality1 + "</option>");
                    }
                    else
                    {
                        htmlBuilder.Append("<option value=\"" + Field.Nationality1 + "\">" + Field.Nationality1 + "</option>");
                    }
                }
                htmlBuilder.Append("</select>");
            }


            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }


        public static IHtmlString CountryDropdown(string value = "")
        {
            var html = "";
            StringBuilder htmlBuilder = new StringBuilder();

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var FieldsList = from Country in db.Countries
                                 select new { Country.CountryID, Country.Country1 };

                htmlBuilder.Append("<select class=\"form-control custom-select\" id=\"Contact\" name=\"Contact\">");
                htmlBuilder.Append("<option value=\"\">Please Select</option>");
                foreach (var Field in FieldsList)
                {

                    if(value == Field.Country1)
                    {
                        htmlBuilder.Append("<option value=\"" + Field.Country1 + "\" selected=\"selected\">" + Field.Country1 + "</option>");
                    }
                    else
                    {
                        htmlBuilder.Append("<option value=\"" + Field.Country1 + "\">" + Field.Country1 + "</option>");
                    }

                    
                }
                htmlBuilder.Append("</select>");
            }


            html = htmlBuilder.ToString();

            return MvcHtmlString.Create(html);
        }



    }



}




