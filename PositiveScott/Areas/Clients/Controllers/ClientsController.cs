﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Clients.Controllers
{
    public class ClientsController : PositiveScott.Controllers.BaseController
    {

        private PositiveNewBuild db = new PositiveNewBuild();


        // GET: Clients/Clients
        public ActionResult Index()
        {

         

            List<Customer> Customer = new List<Customer>();

            var CustomerLookUp = from CU in db.Customers
                                 orderby CU.Surname ascending
                                 select new { CU};


            foreach (var C in CustomerLookUp)
            {
                
                Customer.Add(new Customer {CustomerID = C.CU.CustomerID, FirstName = C.CU.FirstName, Surname = C.CU.Surname, DOB = C.CU.DOB, EmailAddress = C.CU.EmailAddress  });


            }



            return View("Index", Customer);
        }



        public ActionResult Client(int ClientID)
        {


            List<CustomerRecord> CR = new List<CustomerRecord>();

            var Lookup = from CU in db.Customers
                         join CA in db.CustomerAddresses on CU.CustomerID equals CA.CustomerID
                         join CL in db.CustomerLogons on CU.CustomerID equals CL.CustomerID
                         where CU.CustomerID == ClientID
                         select new { CU, CA, CL };


            foreach (var C in Lookup)
            {

                               CR.Add(new CustomerRecord
                {
                   

                    CustomerID = C.CU.CustomerID,
                    Title = C.CU.Title,
                    FirstName = C.CU.FirstName,
                    MiddleNames = C.CU.MiddleNames,
                    Surname = C.CU.Surname,
                    MaidenName = C.CU.MaidenName,
                    PreviousName = C.CU.PreviousName,
                    DOB = C.CU.DOB,
                    Sex = C.CU.Sex,
                    MaritalStatus = C.CU.MaritalStatus,
                    HomeTelephone = C.CU.HomeTelephone,
                    MobileTelephone = C.CU.MobileTelephone,
                    WorkTelephone = C.CU.WorkTelephone,
                    EmailAddress = C.CU.EmailAddress,
                    Nationality = C.CU.Nationality,
                    CountryOfBirth = C.CU.CountryOfBirth,
                    AddressID = C.CA.AddressID,
                    CurrentAddress = C.CA.CurrentAddress,
                    AddressHouseName = C.CA.AddressHouseName,
                    AddressHouseNumber = C.CA.AddressHouseNumber,
                    AddressLine1 = C.CA.AddressLine1,
                    AddressLine2 = C.CA.AddressLine2,
                    AddressLine3 = C.CA.AddressLine3,
                    AddressCounty = C.CA.AddressCounty,
                    AddressPostCode = C.CA.AddressPostCode,
                    MoveInDate = C.CA.MoveInDate,
                    MoveOutDate = C.CA.MoveOutDate,
                    ResidentialStatus = C.CA.ResidentialStatus,
                    CustomerLogonID = C.CL.CustomerID,
                    CustomerLogonUserName = C.CL.UserName,
                    CustomerLockedOut = C.CL.LockedOut,
                    CustomerInitialLogonSent = C.CL.InitialLoginSent,
                    CustomerLastLoginDate = C.CL.LastLoginDate
                }); ;


            }


            return View("Client", CR);
        }







        public ActionResult AddressHistory(int ClientID)
        {




            return View("AddressHistory");
        }

        public ActionResult Applications(int ClientID)
        {


            List<CustomerApplicationsTable> CustomerApps = new List<CustomerApplicationsTable>();

            var Lookup = from D in db.CustomerAppsViews
                         where D.Cust1 == ClientID || D.Cust2 == ClientID || D.Cust3 == ClientID || D.Cust4 == ClientID
                         select new { D };

            foreach (var i in Lookup)
            {

                CustomerApps.Add(new CustomerApplicationsTable {Customer1ID = i.D.Cust1.ToString(), Customer1Name = i.D.Customer1, Customer2ID = i.D.Cust2.ToString(), Customer2Name = i.D.Customer2, Customer3ID = i.D.Cust3.ToString(), Customer3Name = i.D.Customer3, Customer4ID = i.D.Cust4.ToString(), Customer4Name = i.D.Customer4, AppID = i .D.AppID, Lender = "", AccountNum = "", SecurityAddress = "", ProductType = i.D.ProductType, Status = i.D.StatusCode, Advisor = i.D.SLS, CreatedDate = i.D.CreatedDate.ToString()});


            }

            return View("Applications", CustomerApps);
        }


        public ActionResult Dependants(int ClientID)
        {



            return View("Dependants");
        }












        public ActionResult addAddressHistory(PositiveScott.Models.AddAddressHistory History)
        {

            using (var context = new PositiveNewBuild())
            {

                CustomerAddress newAddressHistory = new CustomerAddress()
                {
                    CustomerID = History.CustomerID,
                    AddressHouseName = History.AddressHouseName,
                    AddressHouseNumber = History.AddressHouseNumber,
                    AddressLine1 = History.AddressLine1,
                    AddressLine2 = History.AddressLine2,
                    AddressLine3 = History.Town,
                    AddressCounty = History.County,
                    AddressPostCode = History.PostCode,
                    MoveInDate = History.MoveInDate,
                    MoveOutDate = History.MoveOutDate,
                    CurrentAddress = false

                };

                context.CustomerAddresses.Add(newAddressHistory);
                context.SaveChanges();
            }


        



                return RedirectToAction("Client", "Clients", new { area = "Clients", ClientID = History.CustomerID });
        }









        public ActionResult addNewApplication(PositiveScott.Models.AddApplication Application)

        {
            int NewAppID = 0;


            using (var context = new PositiveNewBuild())
            {




                Application newApplication = new Application()
                {
                    ProductType = Application.ProductType,
                    CreatedUserID = Int32.Parse(Session["UserID"].ToString()),
                    Active = true,


                };
                context.Applications.Add(newApplication);



                CustomerApplication newCustApp = new CustomerApplication()
                {
                    AppID = newApplication.AppID,
                    CustomerID = Application.CustomerID,
                    ApplicantNo = 1,
                    Active = true
                };
                context.CustomerApplications.Add(newCustApp);
                context.SaveChanges();
                NewAppID = newApplication.AppID;

            }


            return RedirectToAction("Case", "Processing", new { AppID = NewAppID, area = ""});

        }








    }
}