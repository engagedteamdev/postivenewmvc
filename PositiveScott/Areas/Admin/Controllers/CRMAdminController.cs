﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class CRMAdminController : PositiveScott.Controllers.BaseController
    {
        // GET: Admin/CRM
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ThreeSixtyMaintenance()
        {
            return View();
        }


        public ActionResult OutstandingDocumensMaintenance()
        {
            return View();
        }

        public ActionResult SuitabilityMaintenance()
        {
            return View();
        }

        public ActionResult SignatureMaintenance()
        {
            return View();
        }



        public ActionResult DashboardCompletionTargets()
        {
            return View();
        }

        public ActionResult DashboardQuarterlyTargets()
        {
            return View();
        }

        public ActionResult LenderAdmin()
        {
            return View();
        }

        public ActionResult LenderChecklist()
        {
            return View();
        }

        public ActionResult BridgingProcFees()
        {
            return View();
        }

        public ActionResult FAQAdmin()
        {
            return View();
        }





    }
}