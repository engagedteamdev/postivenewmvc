﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class StaffAdminController : PositiveScott.Controllers.BaseController
    {
        // GET: Admin/Staff
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserMaintenance()
        {
            return View();
        }

        public ActionResult ActiveUsers()
        {
            return View();

        }

        public ActionResult LoginAttempts()
        {
            return View();
        }

        public ActionResult OutOfOfficeLogs()
        {
            return View();
        }
    }
}