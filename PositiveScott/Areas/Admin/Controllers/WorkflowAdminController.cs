﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class WorkflowAdminController : PositiveScott.Controllers.BaseController
    {
        // GET: Admin/Workflow
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Maintenance()
        {
            return View();
        }


        public ActionResult RecycleStrategies()
        {
            return View();
        }

        public ActionResult Rotas()
        {
            return View();
        }
    }
}