﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class CaseProcessingAdminController : PositiveScott.Controllers.BaseController
    {

        private PositiveNewBuild db = new PositiveNewBuild();

        // GET: Admin/CaseProcessingAdmin
        public ActionResult PanelMaintenance()
        {


   

            List<PanelMaintenance> PanelLookup = new List<PanelMaintenance>();
            var PanelImportLookup = from APM in db.ApplicationPageMappings
                                    join IM in db.ImportValidations on APM.ImportValID equals IM.ValID
                                    where APM.PageID == 1
                                    orderby APM.PageMappingID
                                    select new { IM, APM };


      
            foreach (var I in PanelImportLookup)
            {
                PanelLookup.Add(new PanelMaintenance { ImportValID = I.IM.ValID, ImportValName = I.IM.ValName, InUse = true});
            }



           






            return View("", PanelLookup);
        }

        public ActionResult PanelTemplateMaintenance(int PanelID)
        {

            
















            List<ImportValidation> NotInUse = new List<ImportValidation>();









            return View();
        }






        public ActionResult PluginMaintenance()
        { 
            List<ImportValidation> ImpList = new List<ImportValidation>();
            var ImportLookup = db.ImportValidations.ToList();
            foreach (var I in ImportLookup)
            {


                ImpList.Add(new ImportValidation { ValName = I.ValName, ValID = I.ValID, PluginType = I.PluginType, ValType = I.ValType });
           
              


            }




            return View("", ImpList);

;
        }

        public ActionResult ProcessingScreenMaintenance()
        {
            return View();
        }
    }
}