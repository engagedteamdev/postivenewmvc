﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class DataAdminController : PositiveScott.Controllers.BaseController
    {
        // GET: Admin/Data
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult DataImporter()
        {
            return View();
        }
        public ActionResult DataImporterSchedules()
        {
            return View();
        }
        public ActionResult DataLogs()
        {
            return View();
        }
        public ActionResult QueryLogs()
        {
            return View();
        }
        public ActionResult LockedCases()
        {
            return View();
        }
        public ActionResult WebsiteLogs()
        {
            return View();
        }

    }
}