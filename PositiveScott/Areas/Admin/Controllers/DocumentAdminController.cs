﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Admin.Controllers
{
    public class DocumentAdminController : PositiveScott.Controllers.BaseController
    {
        // GET: Admin/Document
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LetterTemplate()
        {
            return View();
        }

        public ActionResult LetterParagraph()
        {
            return View();
        }

        public ActionResult PDFMaintenance()
        {
            return View();
        }

        public ActionResult EmailTemplate()
        {
            return View();
        }

    }
}