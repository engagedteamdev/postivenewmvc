﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Brokers.Controllers
{
    public class BrokersController : PositiveScott.Controllers.BaseController
    {
        // GET: Brokers/Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewBrokerReg()
        {
            return View();
        }

        public ActionResult NewBrokers()
        {
            return View();
        }

        public ActionResult ProspectBrokers()
        {
            return View();
        }

        public ActionResult ActiveBrokers()
        { 
            return View();
        }

        public ActionResult KeyBrokers()
        {
            return View();
        }

        public ActionResult VerifyBrokers()
        {
            return View();
        }





        public ActionResult ContactList()
        {

            return View();
        }


        public ActionResult NetworkList()
        {
            return View();
        }



        public ActionResult NewNetworkReg()
        {
            return View();
        }

        public ActionResult NewContactReg()
        {
            return View();
        }


        public ActionResult Appointments()
        {
            return View();
        }

        public ActionResult UpdateBrokerDetails()
        {
            return View();
        }
    }
}