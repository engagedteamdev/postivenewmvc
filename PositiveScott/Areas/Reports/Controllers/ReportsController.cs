﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Areas.Reports
{
    public class ReportsController : PositiveScott.Controllers.BaseController
    {

        private PositiveNewBuild db = new PositiveNewBuild();

        // GET: Reports/Reports
        public ActionResult Index()
        {

            List<ReportBuilder> ReportBuilder = new List<ReportBuilder>();

            var ApplicationLookUp = from App in db.Applications
                                    join CA in db.CustomerApplications on App.AppID equals CA.AppID
                                    join CU in db.Customers on CA.CustomerID equals CU.CustomerID
                                    where CA.Active == true
                                    orderby App.CreatedDate descending
                                 select new { App, CU };


            foreach (var A in ApplicationLookUp)
            {

                ReportBuilder.Add(new ReportBuilder { AppID =  A.App.AppID, StatusCode = A.App.StatusCode, SubStatusCode = A.App.SubStatusCode, CreatedDate = A.App.CreatedDate, DialAttempts = A.App.DialAttempts, ProductType = A.App.ProductType, FirstName = A.CU.FirstName, Surname = A.CU.Surname, DOB = A.CU.DOB, EmailAddress = A.CU.EmailAddress});


            }

            return View("Index", ReportBuilder);
        }
    }
}