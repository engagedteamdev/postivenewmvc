
function budgetPlanner() {  

	budgetCals()
	
	
	
	$('#LoansHP').change(function() { budgetCals(); });
	$('#CreditCards').change(function() { budgetCals();});
	$('#UtilityBills').change(function() { budgetCals(); });
	$('#CouncilTax').change(function() { budgetCals();});
	$('#PhonesInternetTV').change(function() { budgetCals(); });
	$('#PetrolTransport').change(function() { budgetCals();});
	
	$('#CarInsurance').change(function() { budgetCals(); });
	$('#FoodShopping').change(function() { budgetCals();});
	$('#ClothesBeauty').change(function() { budgetCals(); });
	$('#InvestmentPensionsSavings').change(function() { budgetCals();});
	$('#ProtectionInsurance').change(function() { budgetCals();});
	
	
	
	$('#HomeInsurance').change(function() { budgetCals();});
	$('#EntertainmentHolidays').change(function() { budgetCals();});
	$('#OtherRegSubs').change(function() { budgetCals();});
	$('#OtherItems').change(function() { budgetCals();});
	
	$('#TotalNetMonthlyIncome').change(function() { budgetCals();});
	//$('#TotalExpenditure').change(function() { budgetCals();});


    $('#LoansHP').attr('readonly', true);
    $('#CreditCards').attr('readonly', true);
    $('#TotalExpenditure').attr('readonly', true);
    $('#AmountAvailableForMortgage').attr('readonly', true);


	budgetCals()
}


function budgetCals() {
	
	$LoanHP = parseFloat(document.getElementById("LoansHP").value);

	if (isNaN($LoanHP))
	{
		$LoanHP = 0
		$('#LoansHP').val($LoanHP);
	}
	
	$CreditCard = parseFloat(document.getElementById("CreditCards").value);
	if (isNaN($CreditCard))
	{
		$CreditCard = 0
		$('#CreditCards').val($CreditCard);
	}
	
	$Utility = parseFloat(document.getElementById("UtilityBills").value);
	if (isNaN($Utility))
	{
		$Utility = 0
		$('#UtilityBills').val($Utility);
	}
	
	
	$CouncilTax = parseFloat(document.getElementById("CouncilTax").value);
	if (isNaN($CouncilTax))
	{
		$CouncilTax = 0
		$('#CouncilTax').val($CouncilTax);
	}
  	
	
	$Phones = parseFloat(document.getElementById("PhonesInternetTV").value);
	if (isNaN($Phones))
	{
		$Phones = 0
		$('#PhonesInternetTV').val($Phones);
	}
 
 	$Petrol = parseFloat(document.getElementById("PetrolTransport").value);
	if (isNaN($Petrol))
	{
		$Petrol = 0
		$('#PetrolTransport').val($Petrol);
	}
 	
 
 	$CarInsurance = parseFloat(document.getElementById("CarInsurance").value);
	if (isNaN($CarInsurance))
	{
		$CarInsurance = 0
		$('#CarInsurance').val($CarInsurance);
	}
	  

	$Food = parseFloat(document.getElementById("FoodShopping").value);
	if (isNaN($Food))
	{
		$Food = 0
		$('#FoodShopping').val($Food);
	}  
	  
	$clothing = parseFloat(document.getElementById("ClothesBeauty").value);
	if (isNaN($clothing))
	{
		$clothing = 0
		$('#ClothesBeauty').val($clothing);
	} 
	  
	  
	$Investment = parseFloat(document.getElementById("InvestmentPensionsSavings").value);
	if (isNaN($Investment))
	{
		$Investment = 0
		$('#InvestmentPensionsSavings').val($Investment);
	}    
	  
	$Protection = parseFloat(document.getElementById("ProtectionInsurance").value);
	if (isNaN($Protection))
	{
		$Protection = 0
		$('#ProtectionInsurance').val($Protection);
	}  
	    
	
	$HomeInsurance = parseFloat(document.getElementById("HomeInsurance").value);
	if (isNaN($HomeInsurance))
	{
		$HomeInsurance = 0
		$('#HomeInsurance').val($HomeInsurance);
	}
	
	$Entertainment = parseFloat(document.getElementById("EntertainmentHolidays").value);
	if (isNaN($Entertainment))
	{
		$Entertainment = 0
		$('#EntertainmentHolidays').val($Entertainment);
	}
	
	$OtherSub = parseFloat(document.getElementById("OtherRegSubs").value);
	if (isNaN($OtherSub))
	{
		$OtherSub = 0
		$('#OtherRegSubs').val($OtherSub);
	}
	
	$OtherItems = parseFloat(document.getElementById("OtherItems").value);
	if (isNaN($OtherItems))
	{
		$OtherItems = 0
		$('#OtherItems').val($OtherItems);
	}
	
	$TotalExpenditure = parseFloat($LoanHP + $CreditCard +  $Utility + $CouncilTax + $Phones + $Petrol + $CarInsurance + $Food + $clothing + $Investment + $Protection + $HomeInsurance + $Entertainment + $OtherSub + $OtherItems)
 
  
  	$("#TotalExpenditure").val($TotalExpenditure);
	saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $("#TotalExpenditure"))	
    
	//alert($totalInc);  		
	
	$monthlyExp = parseFloat($TotalExpenditure).toFixed(2);  
	$weeklyExp = parseFloat($TotalExpenditure / 4).toFixed(2); 
	$yearlyExp = parseFloat($TotalExpenditure * 12).toFixed(2);  
	$("#MonthlyExp").val($monthlyExp);  
	$("#WeeklyExp").val($weeklyExp); 
	$("#YearlyExp").val($yearlyExp); 
		
	
	
	$TotalIncome = parseFloat(document.getElementById("TotalNetMonthlyIncome").value);
	if (isNaN($TotalIncome))
	{
		$TotalIncome = 0
		$('#TotalNetMonthlyIncome').val($TotalIncome);
	}
	
	
	$AmountAvailable = parseFloat($TotalIncome - $TotalExpenditure);
	$("#AmountAvailableForMortgage").val($AmountAvailable);
	saveField($('#frmAppID').val(), $('#MediaCampaignID').val(), $("#AmountAvailableForMortgage"))
	
	//$monthlyInc = Math.round($totalInc / 12);  
//	$weeklyInc = Math.round($totalInc / 52); 
//	$yearlyInc = Math.round($totalInc); 
	

	
	//$("#MonthlyInc").val($monthlyInc);  
//	$("#WeeklyInc").val($weeklyInc); 
//	$("#YearlyInc").val($yearlyInc); 	
//	
//	$("#MonthlyDis").val(Math.round($monthlyInc - $monthlyExp));
//	$("#WeeklyDis").val(Math.round($weeklyInc - $weeklyExp)); 
//	$("#YearlyDis").val(Math.round($yearlyInc - $yearlyExp));
	  
  //chart
 
  $LoanHPPerc = ($LoanHP / $TotalExpenditure) * 100
  $CreditCardPerc = ($CreditCard / $TotalExpenditure) * 100
  $UtilityPerc = ($Utility / $TotalExpenditure) * 100
  $CouncilTaxPerc = ($CouncilTax / $TotalExpenditure) * 100
  $PhonesPerc = ($Phones / $TotalExpenditure) * 100
  $PetrolPerc = ($Petrol / $TotalExpenditure) * 100
  $CarInsurancePerc = ($CarInsurance / $TotalExpenditure) * 100
  $FoodPerc = ($Food / $TotalExpenditure) * 100
  $clothingPerc = ($clothing / $TotalExpenditure) * 100  
  $InvestmentPerc = ($Investment / $TotalExpenditure) * 100
  $ProtectionPerc = ($Protection / $TotalExpenditure) * 100
  $HomeInsurancePerc = ($HomeInsurance / $TotalExpenditure) * 100
  $EntertainmentPerc = ($Entertainment / $TotalExpenditure) * 100
  $OtherSubPerc = ($OtherSub / $TotalExpenditure) * 100
  $OtherItemsPerc = ($OtherItems / $TotalExpenditure) * 100
 

  
  
Highcharts.chart('BudgetPlanner', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
	colors: ['#82E0AA','#11A1B2', '#256087', '#35336C', '#3952A0','#653E8A','#723781','#952268','#AF1E65','#C91C62','#D61A61','#E13B4A','#E34047','#F16D29','#fcfc4b','#E6D58A' ],
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
	credits: {
        enabled: false
    },
	exporting: { 
		enabled: false 
	},
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false,              
            }
        }
    },  
    series: [{
        name: 'Expenditure',
        colorByPoint: true,
        data: [
		{ name: 'Loans/HP',y: $LoanHPPerc },
        { name: 'Credit Cards',y: $CreditCardPerc }, 
        { name: 'Utility Bills', y: $UtilityPerc}, 
        { name: 'Council Tax',y: $CouncilTaxPerc },
        { name: 'Phones/Internet',y: $PhonesPerc },
        { name: 'Petrol & Transport',y:$PetrolPerc },
        { name: 'Car Insurance',y: $CarInsurancePerc },
        { name: 'Food Shopping',y: $FoodPerc },
        { name: 'Clothes/Beauty',y: $clothingPerc },
        { name: 'Investment/Pension/Savings',y: $InvestmentPerc },
		{ name: 'Protection Insurance',y: $ProtectionPerc },
		{ name: 'Home Insurance',y: $HomeInsurancePerc },
		{ name: 'Entertainment',y: $EntertainmentPerc },
		{ name: 'Other Subscriptions',y: $OtherSubPerc },
		{ name: 'Other Items',y: $OtherItemsPerc },
        ]
    }]
	
});


 
}
