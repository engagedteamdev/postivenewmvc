//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Medium
    {
        public int MediaID { get; set; }
        public string MediaName { get; set; }
        public string MediaAddressName { get; set; }
        public string MediaAddressLine1 { get; set; }
        public string MediaAddressLine2 { get; set; }
        public string MediaAddressLine3 { get; set; }
        public string MediaAddressLine4 { get; set; }
        public string MediaAddressPostCode { get; set; }
        public string MediaContactFirstname { get; set; }
        public string MediacontactSurname { get; set; }
        public string MediaContactHomeTelephone { get; set; }
        public string MediaContactMobiletelephone { get; set; }
        public string MediaContactEmail { get; set; }
        public bool MediaActive { get; set; }
    }
}
