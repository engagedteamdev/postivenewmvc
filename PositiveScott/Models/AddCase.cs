﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Web.ModelBinding;
using System.Globalization;
using System.ComponentModel.DataAnnotations;

namespace PositiveScott.Models
{
    public class AddCase
    {
        public int CustomerID { get; set; }
        public string Broker { get; set;}
        public string Contact { get; set; }




        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        [Required(ErrorMessage = "FirstName is required")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Surname is required")]
       
        public string Surname { get; set; }
        [Required(ErrorMessage = "DOB is required")]
        [DisplayName("Date Of Birth")]
        public DateTime DOB { get; set; }
        [DisplayName("Home Telephone")]
        public string HomeTelephone { get; set; }
        [DisplayName("Mobile Telephone")]
        public string MobileTelephone { get; set; }
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }
        public string PostCode { get; set; }
        [DisplayName("House Name")]
        public string HouseName { get; set; }
        [DisplayName("House Number")]
        public int HouseNumber { get; set; }
        [DisplayName("Address Line One")]
        public string AddressLine1 { get; set; }
        [DisplayName("Address Line Two")]
        public string AddressLine2 { get; set; }
        public string Town { get; set; }
        public string County { get; set; }

        public string ProductType { get; set; }
        public string Source { get; set; }

        public List<ImportValidation> ImportValidations { get; set; }
    }
}