﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PositiveScott.Models
{
    public class CustomerApplicationsTable
    {

        public string Customer1ID { get; set; }
        public string Customer1Name { get; set; }
        public string Customer2ID { get; set; }
        public string Customer2Name { get; set; }
        public string Customer3ID { get; set; }
        public string Customer3Name { get; set; }
        public string Customer4ID { get; set; }
        public string Customer4Name { get; set; }
        public int AppID { get; set; }
        public string Lender { get; set; }
        public string AccountNum { get; set; }
        public string SecurityAddress { get; set; }
        public string ProductType { get; set; }
        public string Status { get; set; }
        public string Advisor { get; set; }
        public string CreatedDate { get; set ;}

    }
}