//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SystemConfiguration
    {
        public int SystemConfigurationID { get; set; }
        public string SystemConfigurationName { get; set; }
        public string SystemConfigurationValue { get; set; }
        public Nullable<bool> SystemConfigurationActive { get; set; }
    }
}
