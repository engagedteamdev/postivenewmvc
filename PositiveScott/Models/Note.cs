//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Note
    {
        public int NoteID { get; set; }
        public int ReferenceID { get; set; }
        public string Reference { get; set; }
        public string Note1 { get; set; }
        public int CreatedUserID { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool NoteActive { get; set; }
        public int NoteType { get; set; }
    }
}
