﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace PositiveScott.Models
{
    public class CustomerRecord
    {
        public int CustomerID { get; set; }
        public string Title { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Middle Name")]
        public string MiddleNames { get; set; }
        [DisplayName("Surname Name")]
        public string Surname { get; set; }
        [DisplayName("Maiden Name")]
        public string MaidenName { get; set; }
        [DisplayName("Previous Name")]
        public string PreviousName { get; set; }
        [DisplayName("Date of Birth")]
        public Nullable<System.DateTime> DOB { get; set; }

        public string Sex { get; set; }
        [DisplayName("Marital Status")]
        public string MaritalStatus { get; set; }
        [DisplayName("Home Telephone")]
        public string HomeTelephone { get; set; }
        [DisplayName("Mobile Telephone")]
        public string MobileTelephone { get; set; }
        [DisplayName("Work Telephone")]
        public string WorkTelephone { get; set; }
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }
        public string Nationality { get; set; }
        [DisplayName("Country of Birth")]
        public string CountryOfBirth { get; set; }

        public int AddressID { get; set; }

        public bool CurrentAddress { get; set; }
        [DisplayName("House Name")]
        public string AddressHouseName { get; set; }
        [DisplayName("House Number")]
        public Nullable<int> AddressHouseNumber { get; set; }
        [DisplayName("Address Line 1")]
        public string AddressLine1 { get; set; }
        [DisplayName("Address Line 2")]
        public string AddressLine2 { get; set; }
        [DisplayName("Town")]
        public string AddressLine3 { get; set; }
        [DisplayName("County")]
        public string AddressCounty { get; set; }
        [DisplayName("Post Code")]
        public string AddressPostCode { get; set; }
        [DisplayName("Move In Date")]
        public Nullable<System.DateTime> MoveInDate { get; set; }
        [DisplayName("Move Out Date")]
        public Nullable<System.DateTime> MoveOutDate { get; set; }
        [DisplayName("Residential Status")]
        public string ResidentialStatus { get; set; }
        public Nullable<bool> Active { get; set; }





        public int CustomerLogonID { get; set; }
        [DisplayName("Username")]
        public string CustomerLogonUserName { get; set; }
        [DisplayName("Last Login Date")]
        public Nullable<System.DateTime> CustomerLastLoginDate { get; set; }
        public Nullable<bool> CustomerLockedOut { get; set; }
        public Nullable<bool> CustomerActive { get; set; }
        public Nullable<bool> CustomerInitialLogonSent { get; set; }






    }
}