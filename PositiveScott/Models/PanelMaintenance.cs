﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PositiveScott.Models
{
    public class PanelMaintenance
    {


        public int ImportValID { get; set; }
        public string ImportValName { get; set; }
        public int ImportValType { get; set; }

        public bool InUse { get; set; }

        public int Order { get; set; }
    }
}