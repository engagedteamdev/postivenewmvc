//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ApplicationStatusHistory
    {
        public int StatusHistoryID { get; set; }
        public Nullable<int> AppID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string StatusCode { get; set; }
        public string SubStatusCode { get; set; }
        public Nullable<System.DateTime> StatusDate { get; set; }
    }
}
