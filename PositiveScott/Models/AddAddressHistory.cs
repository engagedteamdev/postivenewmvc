﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PositiveScott.Models
{
    public class AddAddressHistory
    {


        public int CustomerID { get; set; }

        public string AddressHouseName { get; set; }
        public int AddressHouseNumber { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string Town { get; set; }

        public string County { get; set; }

        public string PostCode { get; set; }

        public DateTime MoveInDate { get; set; }

        public DateTime MoveOutDate { get; set; }

    }
}