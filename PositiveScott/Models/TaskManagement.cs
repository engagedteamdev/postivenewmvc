//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TaskManagement
    {
        public int TaskID { get; set; }
        public string StatusTrigger { get; set; }
        public string SubStatusTrigger { get; set; }
        public string TaskDescription { get; set; }
        public string TaskType { get; set; }
        public string AssignedTo { get; set; }
        public Nullable<bool> Scheduled { get; set; }
        public Nullable<int> StartDateOffSetDays { get; set; }
        public Nullable<int> StartTimeOffSetHours { get; set; }
        public Nullable<int> StartTimeOffSetMins { get; set; }
        public Nullable<int> EndDateOffSetDays { get; set; }
        public Nullable<int> EndDateOffSetHours { get; set; }
        public Nullable<int> EndTimeOffSetMins { get; set; }
        public string Priority { get; set; }
        public string EmailTrigger { get; set; }
        public string SMSTrigger { get; set; }
        public Nullable<bool> Active { get; set; }
    }
}
