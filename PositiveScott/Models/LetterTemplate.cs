//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class LetterTemplate
    {
        public int LetterID { get; set; }
        public string LetterName { get; set; }
        public int LetterType { get; set; }
        public string LetterTitle { get; set; }
        public string LetterBody { get; set; }
        public string LetterBackground { get; set; }
        public string LetterColour { get; set; }
        public bool LetterActive { get; set; }
        public int CompanyID { get; set; }
        public string LetterEmailAttachments { get; set; }
        public int LetterEmailTemplateID { get; set; }
        public int LetterEmailProfileID { get; set; }
        public bool LetterEmailReplyTo { get; set; }
        public Nullable<bool> LetterInPack { get; set; }
        public bool LetterEditable { get; set; }
    }
}
