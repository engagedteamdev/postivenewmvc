//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PositiveScott.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Dashboard
    {
        public int DashboardID { get; set; }
        public string DashboardName { get; set; }
        public bool DashboardActive { get; set; }
        public int DashboardRefreshMS { get; set; }
        public string DashboardURL { get; set; }
    }
}
