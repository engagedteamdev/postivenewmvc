﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PositiveScott.Models
{
    public class ReportBuilder
    {


        public string FirstName { get; set; }
        public string Surname { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string EmailAddress { get; set; }
        public int AppID { get; set; }
        public string StatusCode { get; set; }
        public string SubStatusCode { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> DialAttempts { get; set; }
        public string ProductType { get; set; }
    }
}