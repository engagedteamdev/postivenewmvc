﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls.Expressions;

namespace PositiveScott.Controllers
{
    public class ProcessingController : BaseController
    {


        public class UpdateField
        {
            public string AppID { get; set; }
            public string ValID { get; set; }
            public string Value { get; set; }
        }






        // GET: Processing
        public ActionResult Case(int AppID)
        {


            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                
                List<ImportValidation> ImportVal = new List<ImportValidation>();

                var FieldsList = from AT in db.ApplicationTemplates
                                 join AP in db.ApplicationPages on AT.TemplateID equals AP.TemplateID
                                 join PM in db.ApplicationPageMappings on AP.PageID equals PM.PageID
                                 join IM in db.ImportValidations on PM.ImportValID equals IM.ValID
                                 where AP.PageOrder == 0
                                 orderby PM.PageMappingID ascending
                                 select new { IM };
                                


                foreach (var Field in FieldsList)
                {

                    if (Field.IM.ValType == "2")
                    {
                        ImportVal.Add(new ImportValidation { ValName = Field.IM.ValName, 
                                                             ValID = Field.IM.ValID, 
                                                             ValType = Field.IM.ValType, 
                                                             DropdownValues = Field.IM.DropdownValues.ToString(), 
                                                             PluginType = Field.IM.PluginType
                                                            });
                        
                    }
                    else
                    {
                        ImportVal.Add(new ImportValidation { ValName = Field.IM.ValName, 
                                                             ValID = Field.IM.ValID, 
                                                             ValType = Field.IM.ValType, 
                                                             DropdownValues = "", 
                                                             PluginType = Field.IM.PluginType, 
                                                             FriendlyName = Field.IM.FriendlyName.ToString(), 
                                                             Class = Field.IM.Class.ToString()

                                                             


                                                             
                           
                                                            });
                    }

                    
                }

                return View("", ImportVal);
            }

        }





        public ActionResult ExitCase(int AppID)
        {






            return RedirectToAction("Index", "Home", new { @area = "" });
        }


        public ActionResult CaseDetails(int AppID)
        {



            return View("CaseDetails");
        }


        public ActionResult CaseActions(int AppID)
        {



            return View("CaseActions");
        }








    }
}