﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Controllers
{
    public class NotificationsController : BaseController
    {
        // GET: Notifications
        public ActionResult Index()
        {

            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var userid = Int32.Parse(Session["UserID"].ToString());

                List<Task> TaskList = new List<Task>();
                var TaskLookup = db.Tasks.Where(y => (y.AssignedTo == userid)).ToList();
                foreach (var item in TaskLookup)
                {
                    TaskList.Add(new Task {TaskID = item.TaskID, AppID = item.AppID, AssignedBy = item.AssignedBy, AssignedTo = item.AssignedTo, Title = item.Title, TaskType = item.TaskType, Description = item.Description, CalendarID = item.CalendarID, StartDate = item.StartDate, EndDate = item.EndDate, ContactID = item.ContactID  });
                }
                return View("Index");




            }

        }
    }
}