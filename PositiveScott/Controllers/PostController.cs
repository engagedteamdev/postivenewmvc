﻿using Newtonsoft.Json.Linq;
using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls.Expressions;

namespace PositiveScott.Controllers
{
    public class PostController : Controller
    {


        public class UpdateField
        {
            public string AppID { get; set; }
            public string FieldName { get; set; }
            public string Value { get; set; }
        }

        // GET: Post
        [HttpPost]
        public JsonResult SaveField(UpdateField data)
        {
            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                int? AppID = Convert.ToInt32(data.AppID);
                var customerIDs = from CA in db.CustomerApplications
                                  where CA.Active == true && CA.AppID == AppID
                                  select new { CA.CustomerID };
                string FirstName = data.Value;
                foreach (var customerID in customerIDs.ToList())
                {
                    if (customerID.CustomerID.Value.ToString() == null)
                    {
                        Customer NewCustomer = new Customer();
                        NewCustomer.FirstName = FirstName;
                        db.Customers.Add(NewCustomer);
                    }
                    else
                    {
                        var existngCustomer = db.Customers.Where(x => x.CustomerID == customerID.CustomerID.Value).FirstOrDefault();
                        existngCustomer.FirstName = data.Value.ToString();
                        db.SaveChanges();
                    }
                }
                return null;
            }
        }
    }
}