﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;

namespace PositiveScott.Controllers
{
    public class LoginController : Controller
    {


        private PositiveNewBuild db = new PositiveNewBuild();


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authorise(PositiveScott.Models.User userModel)
        {


                var userDetails = db.Users.Where(x => x.UserName == userModel.UserName).FirstOrDefault();

                if(userDetails == null)
                {
                    userModel.UserLoginErrorMessage = "Invalid Username";
                    return View("Index", userModel);
                }
                else
                {
                    var UserPassword = HashHMAC(userModel.UserName.ToLower(), userModel.UserPassword);
                    

                    if(userDetails.UserActive == false)
                    {
                        LoginAttempt attempt = new LoginAttempt();
                        attempt.LoginAttemptUserID = userDetails.UserID;
                        attempt.LoginAttemptUserName = userDetails.UserName;
                        attempt.LoginAttemptDate = DateTime.Now;
                        attempt.LoginAttemptSuccess = false;
                        attempt.LoginAttemptIPAddress = Request.ServerVariables["REMOTE_ADDR"];
                        db.LoginAttempts.Add(attempt);


                        AuditLog Log = new AuditLog();
                        Log.AuditUserID = userDetails.UserID;
                        Log.AuditDate = DateTime.Now;
                        Log.AuditTypeID = 16;
                        Log.AuditDescription = "Login Attempt";
                        db.AuditLogs.Add(Log);

                        userDetails.UserLoginAttempts++;
                        db.SaveChanges();



                        userModel.UserLoginErrorMessage = "Account Inactive - Please contact Administrator";
                        return View("Index", userModel);

                    }
                    else
                    {
                    if (userDetails.UserPassword != UserPassword)
                    {

                        LoginAttempt attempt = new LoginAttempt();
                        attempt.LoginAttemptUserID = userDetails.UserID;
                        attempt.LoginAttemptUserName = userDetails.UserName;
                        attempt.LoginAttemptDate = DateTime.Now;
                        attempt.LoginAttemptSuccess = false;
                        attempt.LoginAttemptIPAddress = Request.ServerVariables["REMOTE_ADDR"];
                        db.LoginAttempts.Add(attempt);


                        AuditLog Log = new AuditLog();
                        Log.AuditUserID = userDetails.UserID;
                        Log.AuditDate = DateTime.Now;
                        Log.AuditTypeID = 16;
                        Log.AuditDescription = "Login Attempt";
                        db.AuditLogs.Add(Log);


                        userDetails.UserLoginAttempts++;
                        db.SaveChanges();


                        int CurrentCount = userDetails.UserLoginAttempts.Value + 1;

                        if (CurrentCount >= 5)
                        {


                            AuditLog NewLog = new AuditLog();
                            NewLog.AuditUserID = userDetails.UserID;
                            NewLog.AuditDate = DateTime.Now;
                            NewLog.AuditTypeID = 17;
                            NewLog.AuditDescription = "Account Locked";
                            db.AuditLogs.Add(Log);

                            userDetails.UserLockedOut = true;
                            db.SaveChanges();


                            userModel.UserLoginErrorMessage = "Account Locked Out, Please contact Administrator";
                            return View("Index", userModel);
                        }
                        else
                        {
                            userModel.UserLoginErrorMessage = "Invalid Password";
                            return View("Index", userModel);

                        }
                        
                    }
                    else
                    {
                        LoginAttempt attempt = new LoginAttempt();
                        attempt.LoginAttemptUserID = userDetails.UserID;
                        attempt.LoginAttemptUserName = userDetails.UserName;
                        attempt.LoginAttemptDate = DateTime.Now;
                        attempt.LoginAttemptSuccess = true;
                        attempt.LoginAttemptIPAddress = Request.ServerVariables["REMOTE_ADDR"];
                        db.LoginAttempts.Add(attempt);


                        AuditLog Log = new AuditLog();
                        Log.AuditUserID = userDetails.UserID;
                        Log.AuditDate = DateTime.Now;
                        Log.AuditTypeID = 3;
                        Log.AuditDescription = "Login";
                        db.AuditLogs.Add(Log);




                        userDetails.UserLoggedIn = true;
                        userDetails.UserLoginAttempts = 0;
                        userDetails.UserSessionID = Session.SessionID;
                        db.SaveChanges();


                        Session["UserID"] = userDetails.UserID;
                        Session["UserFullName"] = userDetails.UserFullName;
                        Session["SessionID"] = userDetails.UserSessionID;
                        Session["TeamID"] = userDetails.UserTeamID;
                        Session["IPAddress"] = Request.ServerVariables["REMOTE_ADDR"];


                        var UserAccess = db.UserAccessLevels.Where(x => x.UserID == userModel.UserID).FirstOrDefault();


                       








                        return RedirectToAction("Index", "Home", new { area = "" });
                    }
                }
                }

        }

        public ActionResult Logout()
        {

            int UserID = Int32.Parse(Session["UserId"].ToString());

            var userDetails = db.Users.Where(x => x.UserID == UserID).FirstOrDefault();

            AuditLog Log = new AuditLog();
            Log.AuditUserID = Int32.Parse(Session["UserId"].ToString());
            Log.AuditDate = DateTime.Now;
            Log.AuditTypeID = 4;
            Log.AuditDescription = "Logout";
            db.AuditLogs.Add(Log);

            userDetails.UserLoggedIn = false;
            userDetails.UserLoginAttempts = 0;
            userDetails.UserSessionID = null;
            db.SaveChanges();
            Session.Abandon();
            return RedirectToAction("Index", "Login");
        }



        public ActionResult ResetPassword(PositiveScott.Models.User userModel)
        {

            var userDetails = db.Users.Where(x => x.UserName == userModel.UserName).FirstOrDefault();

            if (userDetails == null)
            {
                userModel.UserLoginErrorMessage = "Username does not exist";
                return View("Index", userModel);
            }
            else
            {







                userModel.UserLoginErrorMessage = "Password Reset";
                return View("Index", userModel);
            }

                
        }



        private static string HashHMAC(string key, string message)
        {
            var hash = new HMACSHA256(Encoding.UTF8.GetBytes(key));
            Byte[] bhash = hash.ComputeHash(Encoding.UTF8.GetBytes(message));
            return Convert.ToBase64String(bhash);
        }
    }
}