﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Controllers
{
    public class NewCaseController : BaseController
    {
        // GET: NewCase
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult AddCase(PositiveScott.Models.AddCase Case)
        {

            int CustomerID = 0;
            int NewAppID = 0;
            using (var context = new PositiveNewBuild()){

                

                Customer newcust = new Customer() { 
                    Title = Case.Title,
                    FirstName = Case.FirstName,
                    Surname = Case.Surname,
                    DOB = Case.DOB,
                    HomeTelephone = Case.HomeTelephone,
                    MobileTelephone = Case.MobileTelephone,
                    EmailAddress = Case.EmailAddress
                };
                
                context.Customers.Add(newcust);



                CustomerAddress newAddress = new CustomerAddress()
                {
                    CustomerID = newcust.CustomerID,
                    AddressHouseName = Case.HouseName,
                    AddressHouseNumber = Case.HouseNumber,
                    AddressLine1 = Case.AddressLine1,
                    AddressLine2 = Case.AddressLine2,
                    AddressLine3 = Case.Town,
                    AddressCounty = Case.County,
                    AddressPostCode = Case.PostCode,
                    CurrentAddress = true
                };

                context.CustomerAddresses.Add(newAddress);


                Application newApplication = new Application()
                {
                    ProductType = "Mortgage",
                    CreatedUserID = Int32.Parse(Session["UserID"].ToString()),
                    Active = true,
                    BrokerID = Int32.Parse(Case.Broker),
                    ContactID = Int32.Parse(Case.Contact)


                };
                context.Applications.Add(newApplication);



                CustomerApplication newCustApp = new CustomerApplication()
                {
                    AppID = newApplication.AppID,
                    CustomerID = newcust.CustomerID,
                    ApplicantNo = 1,
                    Active = true
                };
                context.CustomerApplications.Add(newCustApp);
                context.SaveChanges();
                CustomerID = newcust.CustomerID;
                NewAppID = newApplication.AppID;

                }
                
                
                
                
                
 

   


            return RedirectToAction("Case", "Processing", new { AppID = NewAppID });








        }
    }
}