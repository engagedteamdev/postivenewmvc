﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PositiveScott.Controllers
{

    using PositiveScott.Resources;

    
       

    public class MessagesController : BaseController
    {



        // GET: Messages
        public ActionResult Index()
        {

            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                var defaultuserid = Int32.Parse(Session["UserID"].ToString());
                List<User> UserList = new List<User>();

                var UserLists = db.Users.Where(m => (m.UserID != defaultuserid && m.UserActive == true && m.UserLockedOut == false)).ToList();
                foreach (var User in UserLists)
                {
                    UserList.Add(new User { UserID = User.UserID, UserFullName = User.UserFullName });
                }

                return View("Index", UserList);
            }

        }

        public ActionResult QuickView()
        {
            using (PositiveNewBuild db = new PositiveNewBuild())
            {

                var userid = Int32.Parse(Session["UserID"].ToString());

                List<Message> MessageList = new List<Message>();

                var MessagesLookup = (from m in db.Messages join u in db.Users on m.MessageFromUserID equals u.UserID where m.MessageToUserID == userid select new { m.MessageFromUserID, m.MessageID, m.MessageSubject, m.MessageType, m.MessageText, m.MessageDate, m.MessageToUserID, m.MessageReadDate, u.UserID, u.UserFullName });
                foreach (var item in MessagesLookup)
                {
                    MessageList.Add(new Message { MessageID = item.MessageID, MessageType = item.MessageType, MessageFromUserID = item.MessageFromUserID, MessageToUserID = item.MessageToUserID, MessageSubject = item.MessageSubject, MessageText = item.MessageText, MessageDate = item.MessageDate, MessageReadDate = item.MessageReadDate });
                }
                return View("QuickView", MessageList);




            }
        }




        public ActionResult Chat(int ChatUserID)
        {

            using (PositiveNewBuild db = new PositiveNewBuild()) {



                var defaultuserid = Int32.Parse(Session["UserID"].ToString());
                List<Message> ChatLog = new List<Message>();
                var ChatLogs = db.Messages.Where(m => ((m.MessageToUserID == defaultuserid && m.MessageFromUserID == ChatUserID) || (m.MessageToUserID == ChatUserID && m.MessageFromUserID == defaultuserid))).OrderByDescending(m => m.MessageDate).ToList();
                foreach (var message in ChatLogs)
                {
                    ChatLog.Add(new Message { MessageID = message.MessageID, MessageDate = message.MessageDate, MessageSubject = message.MessageSubject, MessageText = message.MessageText, MessageReadDate = message.MessageReadDate });
                }




                return View("Chat", ChatLog);
            }
        }

    }
}