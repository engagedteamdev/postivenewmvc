﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;
using PositiveScott.Models;

namespace PositiveScott.Controllers
{
    public class BaseController : Controller
    {
        private bool BoolSuperAdmin = false;



        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool lockedOut = CheckLockedOut(Int32.Parse(Session["UserID"].ToString()));
            bool sessionActive = CheckSessionState(Int32.Parse(Session["UserID"].ToString()), Session["SessionID"].ToString());
            bool blackListed = CheckBlacklist(Session["IPAddress"].ToString());
            if (lockedOut == true || sessionActive == true || blackListed == true)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Login" }, { "action", "Logout" }, { "area", "" } });
                return;
            }
            else
            {
                return;
            }
        
        



        
        
        }



        public static bool CheckBlacklist(string IPAddress)
        {
            bool BlackListStatus = false;
            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                var BlackListCheck = db.Blacklists.Where(x => x.BlackListAddress == IPAddress).ToList();
                if (!BlackListCheck.Any())
                {
                    BlackListStatus = false;
                }
                else{
                    if (BlackListCheck.FirstOrDefault().BlackListActive == true)
                    {
                        BlackListStatus = true;
                    }
                    else
                    {
                        BlackListStatus = false;
                    }
                }
            }
            return BlackListStatus;
        }
        public static bool CheckLockedOut(int UserID)
        {
            bool lockedOut = false;
            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                var lockedOutCheck = db.Users.Where(x => x.UserID == UserID).ToList();

                if (lockedOutCheck.FirstOrDefault().UserLockedOut == true)
                {
                    lockedOut = true;
                }
            }
            return lockedOut;
        }
        public static bool CheckSessionState(int UserID, string SessionID)
        {
            bool sessionActive = false;
            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                var sessionCheck = db.Users.Where(x => x.UserID == UserID).ToList();

                if (sessionCheck.FirstOrDefault().UserSessionID != SessionID)
                {
                    sessionActive = true;
                }
            }
            return sessionActive;
        }





        //////// Config Lookup

        protected static bool assignProperties(string Type, int Userid)
        {
            bool Property = false;
            using (PositiveNewBuild db = new PositiveNewBuild())
            {
                var dbQuery = from U in db.Users
                              join UL in db.UserAccessLevels on U.UserID equals UL.UserID
                              join UT in db.UserAccessTypes on UL.UserAccessLevelID equals UT.UserAccessTypeID
                              where U.UserID == Userid && UT.UserAccessType1 == Type
                              select new { U, UL, UT};
                if (!dbQuery.Any())
                {
                    
                }
                else
                {
                    Property = true;
                }
            }
            return Property;
        }

    }
}