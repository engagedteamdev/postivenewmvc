﻿using PositiveScott.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PositiveScott.Controllers
{
    public class MenuController : BaseController
    {
        private PositiveNewBuild db = new PositiveNewBuild();

        // GET: Menu
        public ActionResult Index()
        {
            string MenuLevels = "";
            int UserID = Int32.Parse(Session["UserID"].ToString());
            var dbPermissionsQry = from U in db.Users
                                   join UL in db.UserAccessLevels on U.UserID equals UL.UserID
                                   join UT in db.UserAccessTypes on UL.UserAccessLevelID equals UT.UserAccessTypeID
                                   where U.UserID == UserID
                                   select new { U, UL, UT };
            if (!dbPermissionsQry.Any())
            {

            }
            else
            {
                foreach (var AT in dbPermissionsQry)
                {
                    if (MenuLevels.Length > 1)
                    {
                        MenuLevels += "|" + AT.UT.UserAccessType1;
               
                    }
                    else
                    {
                        MenuLevels += AT.UT.UserAccessType1;
                       
                    }
                }
      
            }





            List<Menu> MenuList = new List<Menu>();
            var MenuLookups = db.Menus.Where(y => (y.MenuActive == true)).ToList();
            foreach (var M in MenuLookups)
            {

                if (MenuLevels.Contains(M.MenuAdminLevel))
                {
                    MenuList.Add(new Menu { MenuID = M.MenuID, MenuParentObject = M.MenuParentObject, MenuOrder = M.MenuOrder, MenuIcon = M.MenuIcon, MenuName = M.MenuName, MenuRoot = M.MenuRoot, MenuAdminLevel = M.MenuAdminLevel, MenuURL = M.MenuURL, MenuFunction = M.MenuFunction });
                }
                else
                {

                }

                
            }




            return View("Index", MenuList);




          

        }


        public ActionResult InApplication()
        {
            return View("InApplication");
        }
    }
}